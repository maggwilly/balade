package balade.call.service.service.listener;

import balade.call.service.CallServiceApplication;
import balade.call.service.domain.Signal;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Manager;
import org.junit.jupiter.api.*;
import io.socket.client.Socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@SpringBootTest(classes = CallServiceApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "/test.application.properties")
class ConnexionListenerTest {

   final int  TIMEOUT=7000;
    static private Socket socket;
    private  final String ECHO_EVENT_NAME="echo";
    private  final String ECHO_BACK_EVENT_NAME="echoBack";
    @Autowired
    public Configuration rtcConfig;

    @MockBean
    private DisconnectListener disconnectListener;

    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }
    @Timeout(TIMEOUT)
    @DisplayName("Test connexion to server")
    @Test
    public void connectToLocalhost() throws URISyntaxException, InterruptedException {
        doNothing().when(disconnectListener).onDisconnect(any());
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.emit(ECHO_EVENT_NAME,"Eco");
            socket.on(ECHO_BACK_EVENT_NAME, args -> values.offer("done"));
        });
        socket.connect();
        values.take();
        socket.close();
    }
    @AfterEach
    void after() {
        socket.disconnect();
    }


}