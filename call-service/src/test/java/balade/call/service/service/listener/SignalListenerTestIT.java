package balade.call.service.service.listener;

import balade.call.service.CallServiceApplication;
import balade.call.service.domain.UserId;
import balade.call.service.domain.ennum.CallErrorType;
import balade.call.service.domain.ennum.SignalType;
import balade.call.service.service.IClientManager;
import balade.call.service.service.IDataService;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@SpringBootTest(classes = CallServiceApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "/test.application.properties")
class SignalListenerTestIT {
    final int  TIMEOUT=7000;
    private  final String CALL_DATA_EVENT_NAME="callData";
    private  final String ACK_EVENT_NAME="ack";
    private  final String SIGNAL_EVENT_NAME="signal";

    @MockBean
    private IClientManager clientManager;

    @MockBean
    private IDataService dataService;
    @MockBean
    private DisconnectListener disconnectListener;

    @Autowired
    public Configuration rtcConfig;

    static private Socket socket;

    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }


  @Timeout(TIMEOUT)
    @DisplayName("Test Not user found")
    @Test
    void processNotFoundUserData() throws  JSONException, InterruptedException {
        when(dataService.findById(anyString())).thenReturn(null);
        JSONObject signal = new JSONObject();
        signal.put("uniqueId", "s1122");
        signal.put("destId", "dest1");
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.emit(SIGNAL_EVENT_NAME, signal);
            socket.on(ACK_EVENT_NAME, args -> {
                values.offer(args[0]);
            });

        });
        socket.connect();
        JSONObject obj = (JSONObject)values.take();
        assertEquals(obj.get("uniqueId"), signal.get("uniqueId"));
        assertEquals(obj.get("signalType"), SignalType.error.name());
        assertEquals(obj.get("callErrorType"), CallErrorType.NOT_FOUND_DEST.name());
        socket.close();
    }

    @Timeout(TIMEOUT)
    @DisplayName("Test offline User")
    @Test
    void processOfflineUserData() throws  JSONException, InterruptedException {
        UserId userId=new UserId("dest1","cb6a14eb-1c6b-4ed9-9770-7c202d3801e8",false);
        doNothing().when(disconnectListener).onDisconnect(any());
        when(dataService.findById(anyString())).thenReturn(userId);
        when(clientManager.isNotNull(eq(userId))).thenReturn(true);
        JSONObject signal = new JSONObject();
        signal.put("uniqueId", "s1");
        signal.put("destId", "dest1");
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.emit(SIGNAL_EVENT_NAME, signal);
            socket.on(ACK_EVENT_NAME, args -> {
                values.offer(args[0]);
            });

        });
        socket.connect();
        JSONObject obj = (JSONObject)values.take();
        assertEquals(obj.get("uniqueId"), signal.get("uniqueId"));
        assertEquals(obj.get("signalType"), SignalType.error.name());
        assertEquals(obj.get("callErrorType"), CallErrorType.DEST_OFF_LINE.name());
        socket.close();
    }

    @Timeout(TIMEOUT)
    @DisplayName("Test offline User")
    @Test
    void processWithException() throws  JSONException, InterruptedException {
        when(dataService.findById(any())).thenThrow(Exception.class);
        JSONObject signal = new JSONObject();
        signal.put("uniqueId", "s1");
        signal.put("destId", "dest1");
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.emit(SIGNAL_EVENT_NAME, signal);
            socket.on(ACK_EVENT_NAME, args -> {
                values.offer(args[0]);
            });

        });
        socket.connect();
        JSONObject obj = (JSONObject)values.take();
        assertEquals(obj.get("uniqueId"), signal.get("uniqueId"));
        assertEquals(obj.get("signalType"), SignalType.error.name());
        assertEquals(obj.get("callErrorType"), CallErrorType.UNABLE_TO_PROCESS.name());
        socket.close();
    }

    @Timeout(TIMEOUT)
    @DisplayName("Test processData")
    @Test
    void sentSignalToServer() throws  JSONException, InterruptedException {
        SocketIOClient client = spy(SocketIOClient.class);
        doNothing().when(client).sendEvent(anyString(),any());
        when(dataService.findById(any())).thenReturn(new UserId("dest1","cb6a14eb-1c6b-4ed9-9770-7c202d3801e8",false));
        when(clientManager.getClient(any())).thenReturn(client);
        when(clientManager.isNotNull(any())).thenReturn(true);
        JSONObject signal = new JSONObject();
        signal.put("uniqueId", "s1");
        signal.put("destId","dest1");
        final BlockingQueue<Object> values = new LinkedBlockingQueue<>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.emit(SIGNAL_EVENT_NAME, signal);
            socket.on(ACK_EVENT_NAME, args -> {
                values.offer(args[0]);
            });
        });
        socket.connect();
        values.take();
        verify(client).sendEvent(eq(CALL_DATA_EVENT_NAME),any());
        socket.close();
    }


    @AfterEach
    void after() {
        socket.disconnect();
    }

}