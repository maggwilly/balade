package balade.call.service;

import balade.call.service.domain.Signal;
import balade.call.service.domain.UserId;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ServerCommandLineRunner implements CommandLineRunner {
    private final Logger log = LoggerFactory.getLogger(ServerCommandLineRunner.class);
    @Autowired
    private ConnectListener connectListener;
    @Autowired
    private DataListener<String>  echoListener;
    @Autowired
    private DisconnectListener disconnectListener;
    @Autowired
    private DataListener<Signal> signalDataListener;
    @Autowired
    private DataListener<UserId> userIdDataListener;

    private final SocketIOServer server;

    @Autowired
    public ServerCommandLineRunner(SocketIOServer server) {
        this.server = server;
    }

    @Override
    public void run(String... args)  {
        server.addDisconnectListener(disconnectListener);
        server.addConnectListener(connectListener);
        server.addEventListener("signal", Signal.class, signalDataListener);
        server.addEventListener("echo",String.class, echoListener);
        server.addEventListener("userId",UserId.class, userIdDataListener);
        server.start();
    }

}
