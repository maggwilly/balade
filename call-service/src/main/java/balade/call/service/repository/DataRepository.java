package balade.call.service.repository;

import balade.call.service.domain.UserId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Repository
public class DataRepository {

    @Autowired
    private RedisTemplate<String, UserId> redisTemplate;

    public void save(UserId data) {
        redisTemplate.opsForValue().set(data.getId(), data);
    }

    public UserId findById(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public List<UserId> findAll(String patten) {
        List<UserId> datas = new ArrayList<>();
        Set<String> keys = redisTemplate.keys(patten);
        Iterator<String> it = keys.iterator();
        while(it.hasNext()){
            datas.add(findById(it.next()));
        }
        return datas;
    }

}
