package balade.call.service.service.impl;

import balade.call.service.domain.UserId;
import balade.call.service.repository.DataRepository;
import balade.call.service.service.IDataService;
import io.lettuce.core.RedisConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserIdService implements IDataService {
    final Logger logger = LoggerFactory.getLogger(UserIdService.class);

    @Autowired
    private DataRepository dataRepository;

    @Override
    public void save(UserId userId) {
        dataRepository.save(userId);
    }

    @Override
    public UserId findById(String key) throws RedisConnectionException {
        return dataRepository.findById(key);
    }

    @Override
    public List<UserId> findFriends(String patten) throws RedisConnectionException {
        logger.debug("findAll data Class [{}] - patten '{}'", UserId.class.getCanonicalName(), patten);
        List<UserId> userIds=new ArrayList<>();
        UserId userId= findById(patten);
        if (userId!=null)
            userId.getUserIds().forEach(id->{
                UserId usrid= findById(id);
                userIds.add(usrid);
            });
        return userIds;
    }

}
