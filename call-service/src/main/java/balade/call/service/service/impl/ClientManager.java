package balade.call.service.service.impl;

import balade.call.service.service.IClientManager;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
public class ClientManager implements IClientManager {
    private final SocketIOServer server;

    public ClientManager(SocketIOServer server) {
        this.server = server;
    }

    @Override
    public SocketIOClient getClient(String sessionId) {
        System.out.println("getClient("+sessionId+");");
        SocketIOClient destCli = server.getClient(UUID.fromString(sessionId));
        return destCli;
    }

    public  Boolean isNotNull(Object object ){
        return object!=null;
    }
}
