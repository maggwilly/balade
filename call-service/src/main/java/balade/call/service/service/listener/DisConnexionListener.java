package balade.call.service.service.listener;

import balade.call.service.domain.UserId;
import balade.call.service.service.IClientManager;
import balade.call.service.service.IDataService;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DisconnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class DisConnexionListener implements DisconnectListener {
    private final Logger log = LoggerFactory.getLogger(DisConnexionListener.class);
    private final IDataService dataService;
    private IClientManager clientManager;
    private  final String OFFLINE_EVENT_NAME="offline";
    public DisConnexionListener(IClientManager clientManager, IDataService dataService) {
        this.dataService = dataService;
        this.clientManager = clientManager;

    }

    @Override
    public void onDisconnect(SocketIOClient client) {
        log.debug("Client[{}] - Disconnected from chat module.", client.getSessionId().toString());
        UserId usr = dataService.findById(client.getSessionId().toString());
        if (clientManager.isNotNull(usr)) {
            usr.setOnline(false);
            dataService.save(usr);
            Iterator<UserId> it = dataService.findFriends(usr.getId()).iterator();
            while (it.hasNext()) {
                UserId next = it.next();
                if (clientManager.isNotNull(next)) {
                    String sessionId=next.getSessionId();
                    if (clientManager.isNotNull(sessionId)){
                        SocketIOClient cli=  clientManager.getClient(sessionId);
                        if(clientManager.isNotNull(cli))
                            cli.sendEvent(OFFLINE_EVENT_NAME,usr);}
                }
            }
        }
    }
}
