package balade.call.service.service;

import balade.call.service.domain.Signal;
import balade.call.service.domain.UserId;
import io.lettuce.core.RedisConnectionException;

import java.util.List;

public interface IDataService {
    public  void  save(UserId userId);
    public UserId findById(String key) throws RedisConnectionException;
    public List<UserId> findFriends(String patten) throws RedisConnectionException;
}
