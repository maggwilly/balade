package balade.call.service.service.listener;



import balade.call.service.domain.UserId;
import balade.call.service.service.IClientManager;
import balade.call.service.service.IDataService;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;


public class UserIdListener implements DataListener<UserId> {
    private final Logger log = LoggerFactory.getLogger(UserIdListener.class);
    private final IClientManager clientManager;
    private final IDataService dataService;
    private  final String ACK_EVENT_NAME="ack";
    private  final String ONELINE_EVENT_NAME="online";
    public UserIdListener(IClientManager clientManager, IDataService dataService) {
        this.clientManager = clientManager;
        this.dataService = dataService;
    }

    @Override
    public void onData(SocketIOClient client, UserId data, AckRequest ackSender) {
        log.debug("Client[{}] - Received chat message '{}'", client.getSessionId().toString(), data.getId());
        data.setOnline(true);
        data.setLastDate(System.currentTimeMillis());
        data.setSessionId(client.getSessionId().toString());
        dataService.save(data);
        Iterator<UserId> it =dataService.findFriends(data.getId()).iterator();
        while(it.hasNext()){
               UserId next=it.next();
            if (clientManager.isNotNull(next)&&next.isOnline()){
                String sessionId=next.getSessionId();
                if (clientManager.isNotNull(sessionId)){
                   SocketIOClient cli=  clientManager.getClient(sessionId);
                   if(clientManager.isNotNull(cli))
                      cli.sendEvent(ONELINE_EVENT_NAME,data);}
             }
        }
        client.sendEvent(ACK_EVENT_NAME,"done");
    }
}
