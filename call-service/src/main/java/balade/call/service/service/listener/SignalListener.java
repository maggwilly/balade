package balade.call.service.service.listener;

import balade.call.service.domain.*;
import balade.call.service.domain.ennum.CallErrorType;
import balade.call.service.service.IClientManager;
import balade.call.service.service.IDataService;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SignalListener implements DataListener<Signal> {
    private final Logger log = LoggerFactory.getLogger(SignalListener.class);
    private final IClientManager clientManager;
    private final IDataService dataService;
    private  final String CALL_DATA_EVENT_NAME="callData";
    private  final String ACK_EVENT_NAME="ack";

    public SignalListener(IClientManager clientManager, IDataService dataService) {
        this.clientManager = clientManager;
        this.dataService = dataService;

    }

    @Override
    public void onData(SocketIOClient client, Signal data, AckRequest ackSender) {
       log.debug("Client[{}] - Received chat message '{}'", client.getSessionId().toString(), data.getDestId());
        try {
            client.sendEvent(ACK_EVENT_NAME, processData(data)); ;
        } catch ( Exception e) {
            processDataWithError(client,e,data);
        }
    }


    public   SignalError processDataWithError(SocketIOClient client,Exception e, Signal data){
         SignalError error = new SignalError(data.getUniqueId());
         error.setErrorMessage(e.getClass().getTypeName()+" :"+e.getMessage());
         if(e instanceof  DataNotFoundException )
             error.setCallErrorType(CallErrorType.NOT_FOUND_DEST);
         else  if (e instanceof  UserOfflineException)
             error.setCallErrorType(CallErrorType.DEST_OFF_LINE);
         else  error.setCallErrorType(CallErrorType.UNABLE_TO_PROCESS);
         client.sendEvent(ACK_EVENT_NAME,error);
       return error;
    }



    public  Signal processData(Signal data) throws  Exception {
        String destSessionId = data.getDestSessionId();
        if (!clientManager.isNotNull(destSessionId)) {
            UserId dest = dataService.findById(data.getDestId());
            if (!clientManager.isNotNull(dest))
                  throw new DataNotFoundException("User Not found");
                destSessionId = dest.getSessionId();
              }
            SocketIOClient destCli = clientManager.getClient(destSessionId);
             if (clientManager.isNotNull(destCli)) {
                destCli.sendEvent(CALL_DATA_EVENT_NAME, data);
                return new SignalAck(data.getUniqueId());
            }
            throw new UserOfflineException("User offline: "+destSessionId);
    }
}