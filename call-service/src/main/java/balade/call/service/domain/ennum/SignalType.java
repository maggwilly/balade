package balade.call.service.domain.ennum;

public enum SignalType {
    call,
    end,
    amswer,
    ignore,
    cancel,
    sdp,
    candidate,
    error,
    ack
}
