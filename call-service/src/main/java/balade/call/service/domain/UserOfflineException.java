package balade.call.service.domain;

public class UserOfflineException extends  Exception {
    public UserOfflineException() {
    }

    public UserOfflineException(String message) {
        super(message);
    }
}
