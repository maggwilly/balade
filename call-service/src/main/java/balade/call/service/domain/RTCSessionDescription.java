package balade.call.service.domain;

import balade.call.service.domain.ennum.RTCSdpType;

import java.io.Serializable;

public class RTCSessionDescription  implements Serializable {
    private RTCSdpType type;
    private String sdp;

    public RTCSdpType getType() {
        return type;
    }

    public void setType(RTCSdpType type) {
        this.type = type;
    }

    public String getSdp() {
        return sdp;
    }

    public void setSdp(String sdp) {
        this.sdp = sdp;
    }
}
