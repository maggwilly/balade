package balade.call.service.domain;

import balade.call.service.domain.ennum.SignalType;

public class SignalAck extends Signal {
    public SignalAck() {
    }
    public SignalAck(String id) {
        super(id);
    }

    public SignalType getSignalType() {
        return SignalType.ack;
    }


}
