package balade.call.service.domain;
import balade.call.service.domain.ennum.SignalType;

import java.io.Serializable;

public class Signal implements Serializable {
     private  String expId;
     private  String destId;
     private  String destSessionId;
     private SignalType signalType;
     private RTCIceCandidate candidate;
     private  RTCSessionDescription sdp;
    private String uniqueId;
    public Signal() {
    }
    public Signal(String id) {
        uniqueId=id;
    }

    public String getExpId() {
        return expId;
    }

    public void setExpId(String expId) {
        this.expId = expId;
    }

    public String getDestId() {
        return destId;
    }

    public void setDestId(String destId) {
        this.destId = destId;
    }

    public String getDestSessionId() {
        return destSessionId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setDestSessionId(String destSessionId) {
        this.destSessionId = destSessionId;
    }

    public SignalType getSignalType() {
        return signalType;
    }

    public void setSignalType(SignalType signalType) {
        this.signalType = signalType;
    }

    public RTCIceCandidate getCandidate() {
        return candidate;
    }

    public void setCandidate(RTCIceCandidate candidate) {
        this.candidate = candidate;
    }

    public RTCSessionDescription getSdp() {
        return sdp;
    }

    public void setSdp(RTCSessionDescription sdp) {
        this.sdp = sdp;
    }
}
