package balade.call.service.domain.ennum;

public enum  CallErrorType {
    NOT_FOUND_DEST,
    DEST_OFF_LINE,
    UNABLE_TO_PROCESS
}
