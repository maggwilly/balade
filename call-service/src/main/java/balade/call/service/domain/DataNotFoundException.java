package balade.call.service.domain;

public class DataNotFoundException extends  Exception {
    public DataNotFoundException() {
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
