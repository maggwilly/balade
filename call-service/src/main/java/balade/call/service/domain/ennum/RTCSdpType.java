package balade.call.service.domain.ennum;

public enum RTCSdpType {
    answer,
    offer,
    pranswer,
    rollback
}
