package balade.call.service.domain;

import java.io.Serializable;

public class RTCIceCandidate implements Serializable {
    private  String candidate;
    private  String component;
    private  String foundation ;
    private  String ip  ;
    private  int port ;
    private  long priority ;
    private  String protocol;
    private  String relatedAddress;
    private  int relatedPort  ;
    private  String sdpMid;
    private  int sdpMLineIndex ;
    private  String tcpType   ;
    private  String type ;
    private  String usernameFragment ;

    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getFoundation() {
        return foundation;
    }

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getRelatedAddress() {
        return relatedAddress;
    }

    public void setRelatedAddress(String relatedAddress) {
        this.relatedAddress = relatedAddress;
    }

    public int getRelatedPort() {
        return relatedPort;
    }

    public void setRelatedPort(int relatedPort) {
        this.relatedPort = relatedPort;
    }

    public String getSdpMid() {
        return sdpMid;
    }

    public void setSdpMid(String sdpMid) {
        this.sdpMid = sdpMid;
    }

    public int getSdpMLineIndex() {
        return sdpMLineIndex;
    }

    public void setSdpMLineIndex(int sdpMLineIndex) {
        this.sdpMLineIndex = sdpMLineIndex;
    }

    public String getTcpType() {
        return tcpType;
    }

    public void setTcpType(String tcpType) {
        this.tcpType = tcpType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsernameFragment() {
        return usernameFragment;
    }

    public void setUsernameFragment(String usernameFragment) {
        this.usernameFragment = usernameFragment;
    }
}
