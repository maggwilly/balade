package balade.call.service.domain;

import balade.call.service.domain.ennum.CallErrorType;
import balade.call.service.domain.ennum.SignalType;

public class SignalError extends Signal {

    private CallErrorType callErrorType;
    private  String errorMessage;
    public SignalError(String id) {
        super(id);
    }


    public CallErrorType getCallErrorType() {
        return callErrorType;
    }

    public void setCallErrorType(CallErrorType callErrorType) {
        this.callErrorType = callErrorType;
    }
    public SignalType getSignalType() {
        return SignalType.error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
