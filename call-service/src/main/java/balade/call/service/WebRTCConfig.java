package balade.call.service;

import balade.call.service.domain.Signal;
import balade.call.service.domain.UserId;
import balade.call.service.service.IClientManager;
import balade.call.service.service.IDataService;
import balade.call.service.service.listener.DisConnexionListener;
import balade.call.service.service.listener.SignalListener;
import balade.call.service.service.listener.UserIdListener;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class WebRTCConfig {
    private final Logger log = LoggerFactory.getLogger(WebRTCConfig.class);

    @Autowired
    public IDataService userIdDataService;

    @Autowired
    public IClientManager clientManager;

    @Bean
    public ConnectListener connectListener(){
        return client -> {
            log.debug("Client[{}] - Connected to server module through namespace '{}'", client.getSessionId());
        };
    }

    @Bean
    public DataListener<UserId> userIdDataListener(){
        return new UserIdListener(clientManager, userIdDataService);
    }

    @Bean
    public DataListener<Signal> signalDataListener(){
        return new SignalListener(clientManager, userIdDataService);
    }

    @Bean
    public DisconnectListener disconnectListener(){
        return new DisConnexionListener(clientManager,userIdDataService);
    }


    @Bean
    private DataListener<String>  echoListener(){
        return (client, data, ackSender) -> {
            log.debug("Client[{}] - echo from client module.  {}", client.getSessionId().toString(),data);
            client.sendEvent("echoBack", data);
        };
    }


}
