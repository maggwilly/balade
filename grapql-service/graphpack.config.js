const config = require("./config/application.properties.json");

module.exports = {
  server: {
    port: config.port,
  },
};