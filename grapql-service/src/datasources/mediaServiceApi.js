
const axios = require('axios');
const config = require("../../config/application.properties.json");

const mdia_url = `http://${config.mediaservice_host}:${config.mediaservice_port}`;
module.exports = axios.create({
    baseURL: mdia_url
});