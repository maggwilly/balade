const mediaService = require('./mediaServiceApi');
export default class MediaService {
    async getResourceByPath(path) {
        const resource = await mediaService.get(`/api/files/${path}`);
        return resource.data;
    }     
}