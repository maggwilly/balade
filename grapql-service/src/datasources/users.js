const dataService = require('./dataServiceApi');
export default class DataService {
    async getUserById(id) {
        const user = await dataService.get(`/api/users/${id}`);
        return user.data;
    }
    async getProfileById(id) {
        const profile = await dataService.get(`/api/profiles/by/user/${id}`);
        return profile.data;
    }  
    async getPictureById(id) {
        const picture = await dataService.get(`/api/pictures/${id}`);
        return picture.data;
    } 

    async getRelationsByExpId(id) {
        const relations = await dataService.get(`/api/relations/by/expId/${id}`);
        return relations.data;
    } 
    
    async getPicturesByProfileId(id) {
        const pictures = await dataService.get(`/api/pictures/by/profile/${id}`);
        return pictures.data;
    } 
    async getReviewsByProfileId(id) {
        const reviews = await dataService.get(`/api/reviews/by/profileId/${id}`);
        return reviews.data;
    }     
}