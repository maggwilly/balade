
const axios = require('axios');
const config = require("../../config/application.properties.json");

const ds_url = `http://${config.dataservice_host}:${config.dataservice_port}`;
module.exports = axios.create({
    baseURL: ds_url
});