// # resolvers.js
import DataService from './datasources/users';
import MediaService from './datasources/medias';

let dataService = new DataService();
let mediaService = new MediaService();
const resolvers = {
    Query: {
        user: async (parent, { id }) => dataService.getUserById(id),
        relations: async (parent, { id }) => dataService.getRelationsByExpId(id),
        pictures:  async (parent, { id }) => dataService.getPicturesByProfileId(id)
    },
    User: {
        profile: async (parent) => dataService.getProfileById(parent.id),
    },
    Relation: {
        user: async (parent) => dataService.getUserById(parent.destId)
    }
   ,
    Profile: {
        avatar: async (parent)=>dataService.getPictureById(parent.id),
        reviews: async (parent) => dataService.getReviewsByProfileId(parent.id)
    },
    
    Picture: {
        resource: async (parent)=>mediaService.getResourceByPath(parent.path),
    }   
}

export default resolvers;