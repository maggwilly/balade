-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: baladedataservice
-- ------------------------------------------------------
-- Server version	8.0.19

/LOCK TABLES `picture` WRITE;
 /*!40000 ALTER TABLE `picture` DISABLE KEYS */;
 INSERT INTO `picture` VALUES (1,'productize',66992,NULL,NULL),(2,'cyan Gloves',2262,NULL,NULL),(3,'brand ADP Functionality',37043,NULL,NULL),(4,'Poland Analyst',93483,NULL,NULL),(5,'Indiana Intranet',89376,NULL,NULL),(6,'Personal Loan Account hacking convergence',67768,NULL,NULL),(7,'Dynamic application',46409,NULL,NULL),(8,'Sports',1620,NULL,NULL),(9,'Steel Health interface',3648,NULL,NULL),(10,'Kids collaboration program',6471,NULL,NULL);
 /*!40000 ALTER TABLE `picture` ENABLE KEYS */;
 UNLOCK TABLES;

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'2020-02-07 02:03:21',82426,'2020-02-07','MALE','ENGLISH','HDD deposit programming','South Garth','THX Managed quantifying','Mouse User-centric Investment Account'),(2,'2020-02-06 18:11:45',49922,'2020-02-06','FEMALE','ENGLISH','Assistant strategy scalable','New Niaburgh','Dynamic','Digitized Shoes Avon'),(3,'2020-02-07 01:57:32',37163,'2020-02-06','MALE','ENGLISH','Accounts','West Irwin','time-frame','next-generation'),(4,'2020-02-06 21:43:20',57860,'2020-02-07','MALE','ENGLISH','application Buckinghamshire','West Sophia','user-centric Awesome','bricks-and-clicks invoice El Salvador'),(5,'2020-02-07 04:34:07',5626,'2020-02-07','MALE','FRENCH','ivory','North Dorcasland','Tactics AI','Row Investment Account'),(6,'2020-02-06 23:06:39',67827,'2020-02-06','MALE','FRENCH','primary granular','Kayleyview','Drives Programmable Handmade Plastic Chicken','communities Manager'),(7,'2020-02-07 09:18:28',26758,'2020-02-07','FEMALE','FRENCH','hacking','Justontown','multi-byte transmit Soap','HDD Intranet Specialist'),(8,'2020-02-07 10:40:22',64530,'2020-02-06','FEMALE','FRENCH','Arkansas Massachusetts Refined Metal Chips','Port Savanah','Towels','indexing Hills Walks'),(9,'2020-02-06 22:53:00',9709,'2020-02-07','FEMALE','ENGLISH','payment Licensed','Lake Noel','collaborative generating strategic','JSON'),(10,'2020-02-07 15:53:53',24380,'2020-02-07','MALE','ENGLISH','Specialist olive','Idellberg','Gourde US Dollar efficient','bypassing harness parsing');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `relation` WRITE;
/*!40000 ALTER TABLE `relation` DISABLE KEYS */;
INSERT INTO `relation` VALUES (1,'2020-02-06 21:14:22',30453,36995,'POSTED'),(2,'2020-02-07 11:50:19',19578,80383,'REFUSED'),(3,'2020-02-07 07:36:27',29032,65226,'POSTED'),(4,'2020-02-07 08:50:01',29624,94002,'REFUSED'),(5,'2020-02-07 01:09:21',65038,97427,'READED'),(6,'2020-02-07 00:20:47',95943,74731,'RECEIVED'),(7,'2020-02-07 14:50:27',59473,13722,'PENDING'),(8,'2020-02-07 02:15:36',58283,69789,'PENDING'),(9,'2020-02-06 22:31:33',29270,69012,'ACCEPTED'),(10,'2020-02-06 20:23:54',96908,33665,'READED');
/*!40000 ALTER TABLE `relation` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'2020-02-06 21:11:46',67162,42502,'DISLIKE'),(2,'2020-02-07 09:19:03',72241,38646,'DISLIKE'),(3,'2020-02-06 23:42:02',67965,80245,'DISLIKE'),(4,'2020-02-07 02:54:06',55095,64866,'DISLIKE'),(5,'2020-02-07 00:44:42',82895,87676,'DISLIKE'),(6,'2020-02-07 01:04:42',81661,52837,'LIKE'),(7,'2020-02-07 08:12:05',14007,25344,'LIKE'),(8,'2020-02-07 09:32:09',61301,72189,'LIKE'),(9,'2020-02-07 06:28:09',29971,91275,'DISLIKE'),(10,'2020-02-07 00:57:13',4065,3382,'LIKE');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'RSS','1-064-095-3441'),(2,'SSL indexing','431.426.5679'),(3,'neutral Kip yellow','376.855.2596'),(4,'Handmade Metal Cheese PNG','444.838.4366 x5246'),(5,'Central payment','1-381-980-7486 x702'),(6,'auxiliary Wooden Buckinghamshire','(478) 765-9665 x4937'),(7,'synergistic Dong system engine','648.006.0983'),(8,'invoice Hungary','192.512.1041 x946'),(9,'gold','(571) 216-8156 x83199'),(10,'grey','601.535.3589 x025');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
