package balade.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class DataServiceApplication {
	@Autowired
	private Environment env;
	public static void main(String[] args) {
		SpringApplication.run(DataServiceApplication.class, args);
	}

	@RequestMapping(path = "/test", method= RequestMethod.GET)
	public String home() {
		return "Spring is here!";
	}
}
