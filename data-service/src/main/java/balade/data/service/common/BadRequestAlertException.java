package balade.data.service.common;

public class BadRequestAlertException extends  Exception {
    private String entityName;
    private ErrorsType errorsType;
    public BadRequestAlertException() {
    }

    public BadRequestAlertException(String message) {
        super(message);
    }

    public BadRequestAlertException(String message,String entityName,ErrorsType errorsType) {
        super(message);
        this.entityName=entityName;
        this.errorsType=errorsType;
    }
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public ErrorsType getErrorsType() {
        return errorsType;
    }

    public void setErrorsType(ErrorsType errorsType) {
        this.errorsType = errorsType;
    }

}
