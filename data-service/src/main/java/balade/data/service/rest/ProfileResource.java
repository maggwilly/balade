package balade.data.service.rest;

import balade.data.service.common.*;
import balade.data.service.domain.Profile;
import balade.data.service.domain.enumeration.Sex;
import balade.data.service.engine.ProfileService;
import balade.data.service.engine.dto.ProfileDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Profile}.
 */
@RestController
@RequestMapping(path="/api")
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    private String applicationName;
    private static final String ENTITY_NAME = "balade.DataService.Profile";

    private final ProfileService profileService;


    public ProfileResource(ProfileService profileService, Environment env) {
        applicationName= env.getProperty("app.name");
        this.profileService = profileService;

    }

    /**
     * {@code POST  /profiles} : Create a new profile.
     * @param profileDTO the profileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new profileDTO, or with status {@code 400 (Bad Request)} if the profile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(path="/profiles")
    public ResponseEntity<ProfileDTO> createProfile(@RequestBody ProfileDTO profileDTO) throws URISyntaxException, BadRequestAlertException {
       log.debug("REST request to save Profile : {}", profileDTO);
        if (profileDTO.getId() != null) {
            throw new BadRequestAlertException("A new profile cannot already have an ID", ENTITY_NAME, ErrorsType.idexists);
        }
        ProfileDTO result = profileService.save(profileDTO);
        return  ResponseEntity.created(new URI("/api/profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /profiles} : Updates an existing profile.
     * @param profileDTO the profileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated profileDTO,
     * or with status {@code 400 (Bad Request)} if the profileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the profileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(path="/profiles")
    public ResponseEntity<ProfileDTO> updateProfile(@RequestBody ProfileDTO profileDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Profile : {}", profileDTO);
        if (profileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, ErrorsType.idnull);
        }
        ProfileDTO result = profileService.save(profileDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, profileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /profiles} : get all the profiles.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of profiles in body.
     */
    @GetMapping(path="/profiles")
    public List<ProfileDTO> getAllProfiles() {
        log.debug("REST request to get all Profiles");
        return profileService.findAll();
    }

    /**
     * {@code GET  /profiles} : get all the profiles by sex and region.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of profiles in body.
     */
    @GetMapping(path="/profiles/by/region/{region}/{sex}")
    public List<ProfileDTO> findAllByRegion(@PathVariable  String region, @PathVariable  Sex sex) {
        log.debug("REST request to get all Profiles");
        return profileService.findAllByRegion(region,sex);
    }


    @GetMapping(path="/profiles/by/place/{place}/{sex}")
    public List<ProfileDTO> findAllByPlace(@PathVariable  String place, @PathVariable  Sex sex) {
        log.debug("REST request to get all Profiles");
        return profileService.findAllByPlace(place,sex);
    }

    @PostMapping(path="/profiles/by/in")
    public ResponseEntity<List<ProfileDTO>> getAllByIdIn(@RequestBody List<Long> ids, @RequestBody Pageable pageable) {
        log.debug("REST request to get a page of Pictures");
        Page<ProfileDTO> page = profileService.findAllByIdIn(ids,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    /**
     * {@code GET  /profiles/:id} : get the "id" profile.
     *
     * @param id the id of the profileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the profileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(path="/profiles/{id}")
    public ResponseEntity<ProfileDTO> getProfile(@PathVariable Long id) {
        log.debug("REST request to get Profile : {}", id);
        Optional<ProfileDTO> profileDTO = profileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(profileDTO);
    }

    @GetMapping(path="/profiles/by/user/{id}")
    public ResponseEntity<ProfileDTO> findOneByUserId(@PathVariable Long id) {
        log.debug("REST request to get Profile : {}", id);
        Optional<ProfileDTO> profileDTO = profileService.findOneByUserId(id);
        return ResponseUtil.wrapOrNotFound(profileDTO);
    }

    /**
     * {@code DELETE  /profiles/:id} : delete the "id" profile.
     *
     * @param id the id of the profileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(path="/profiles/{id}")
    public ResponseEntity<Void> deleteProfile(@PathVariable Long id) {
        log.debug("REST request to delete Profile : {}", id);
        profileService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
