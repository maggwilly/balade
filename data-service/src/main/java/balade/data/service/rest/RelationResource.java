package balade.data.service.rest;

import balade.data.service.domain.Relation;
import balade.data.service.common.*;
import balade.data.service.domain.enumeration.Status;
import balade.data.service.engine.RelationService;
import balade.data.service.engine.dto.RelationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link Relation}.
 */
@RestController
@RequestMapping(path="/api")
public class RelationResource {

    private final Logger log = LoggerFactory.getLogger(RelationResource.class);

    private static final String ENTITY_NAME = "baladeDataServiceRelation";

    private String applicationName;

    private final RelationService relationService;

    public RelationResource(RelationService relationService,Environment env) {
        applicationName=env.getProperty("app.name");
        this.relationService = relationService;
    }

    /**
     * {@code POST  /relations} : Create a new relation.
     *
     * @param relationDTO the relationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new relationDTO, or with status {@code 400 (Bad Request)} if the relation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(path="/relations")
    public ResponseEntity<RelationDTO> createRelation(@RequestBody RelationDTO relationDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save Relation : {}", relationDTO);
        if (relationDTO.getId() != null) {
            throw new BadRequestAlertException("A new relation cannot already have an ID", ENTITY_NAME, ErrorsType.idexists);
        }
        RelationDTO result = relationService.save(relationDTO);
        return ResponseEntity.created(new URI("/api/relations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /relations} : Updates an existing relation.
     *
     * @param relationDTO the relationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated relationDTO,
     * or with status {@code 400 (Bad Request)} if the relationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the relationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(path="/relations")
    public ResponseEntity<RelationDTO> updateRelation(@RequestBody RelationDTO relationDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to update Relation : {}", relationDTO);
        if (relationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, ErrorsType.idnull);
        }
        RelationDTO result = relationService.save(relationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, relationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /relations} : get all the relations.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of relations in body.
     */
    @GetMapping(path="/relations")
    public ResponseEntity<List<RelationDTO>> getAllRelations(Pageable pageable) {
        log.debug("REST request to get a page of Relations");
        Page<RelationDTO> page = relationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping(path="/relations/by/expId/{expId}")
    public ResponseEntity<List<RelationDTO>> findAllByExpId(@PathVariable Long expId,Pageable pageable) {
        log.debug("REST request to get a page of Relations");
        Page<RelationDTO> page = relationService.findAllByExpId(expId,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping(path="/relations/by/expId/{expId}/status/{status}")
    public ResponseEntity<List<RelationDTO>> findAllByExpIdAndStatus(@PathVariable Long expId, @PathVariable Status status,Pageable pageable) {
        log.debug("REST request to get a page of Relations");
        Page<RelationDTO> page = relationService.findAllByExpIdAndStatus(expId,status,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    /**
     * {@code GET  /relations/:id} : get the "id" relation.
     *
     * @param id the id of the relationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the relationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(path="/relations/{id}")
    public ResponseEntity<RelationDTO> getRelation(@PathVariable Long id) {
        log.debug("REST request to get Relation : {}", id);
        Optional<RelationDTO> relationDTO = relationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(relationDTO);
    }

    /**
     * {@code DELETE  /relations/:id} : delete the "id" relation.
     *
     * @param id the id of the relationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(path="/relations/{id}")
    public ResponseEntity<Void> deleteRelation(@PathVariable Long id) {
        log.debug("REST request to delete Relation : {}", id);
        relationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
