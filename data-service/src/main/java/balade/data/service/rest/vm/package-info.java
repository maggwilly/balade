/**
 * View Models used by Spring MVC REST controllers.
 */
package balade.data.service.rest.vm;
