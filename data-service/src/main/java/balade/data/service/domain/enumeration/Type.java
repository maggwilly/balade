package balade.data.service.domain.enumeration;

/**
 * The Type enumeration.
 */
public enum Type {
    TEXT, IMAGE, VIDEO
}
