package balade.data.service.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

import balade.data.service.domain.enumeration.Status;

/**
 * A Relation.
 */
@Entity
@Table(name = "relation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Relation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "exp_id")
    private Long expId;

    @Column(name = "dest_id")
    private Long destId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Relation createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getExpId() {
        return expId;
    }

    public Relation expId(Long expId) {
        this.expId = expId;
        return this;
    }

    public void setExpId(Long expId) {
        this.expId = expId;
    }

    public Long getDestId() {
        return destId;
    }

    public Relation destId(Long destId) {
        this.destId = destId;
        return this;
    }

    public void setDestId(Long destId) {
        this.destId = destId;
    }

    public Status getStatus() {
        return status;
    }

    public Relation status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Relation)) {
            return false;
        }
        return id != null && id.equals(((Relation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Relation{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", expId=" + getExpId() +
            ", destId=" + getDestId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
