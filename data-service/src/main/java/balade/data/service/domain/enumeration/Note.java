package balade.data.service.domain.enumeration;

/**
 * The Note enumeration.
 */
public enum Note {
    LIKE, DISLIKE,HATED,SCARY,DANGEROUS
}
