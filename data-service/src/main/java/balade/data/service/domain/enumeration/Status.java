package balade.data.service.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    ACCEPTED, REFUSED, PENDING, READED, RECEIVED, POSTED
}
