package balade.data.service.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH, ENGLISH
}
