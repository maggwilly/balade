package balade.data.service.domain.enumeration;

/**
 * The Sex enumeration.
 */
public enum Sex {
    MALE, FEMALE
}
