package balade.data.service.engine.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import balade.data.service.domain.Relation;
import balade.data.service.domain.enumeration.Status;

/**
 * A DTO for the {@link Relation} entity.
 */
public class RelationDTO implements Serializable {

    private Long id;

    private ZonedDateTime createdDate;

    private Long expId;

    private Long destId;

    private Status status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getExpId() {
        return expId;
    }

    public void setExpId(Long expId) {
        this.expId = expId;
    }

    public Long getDestId() {
        return destId;
    }

    public void setDestId(Long destId) {
        this.destId = destId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RelationDTO relationDTO = (RelationDTO) o;
        if (relationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), relationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RelationDTO{" +
            "id=" + getId() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", expId=" + getExpId() +
            ", destId=" + getDestId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
