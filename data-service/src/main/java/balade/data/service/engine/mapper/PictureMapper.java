package balade.data.service.engine.mapper;

import balade.data.service.domain.Picture;
import balade.data.service.engine.dto.PictureDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Picture} and its DTO {@link PictureDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PictureMapper extends EntityMapper<PictureDTO, Picture> {


    default Picture fromId(Long id) {
        if (id == null) {
            return null;
        }
        Picture picture = new Picture();
        picture.setId(id);
        return picture;
    }
}
