package balade.data.service.engine.repository;

import balade.data.service.domain.Review;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    Optional<Review> findOneByUserIdAndProfileId(Long expId, Long profileId);
    List<Review> findByProfileId(Long profileId);

    @Query(countQuery ="SELECT COUNT(r.id) FROM review r where r.note = :note AND r.profile_id = :profileId",
            nativeQuery = true)
    Optional<Integer> countByNoteAndProfileId(@Param("note") String note,@Param("profileId") Long profileId);
}
