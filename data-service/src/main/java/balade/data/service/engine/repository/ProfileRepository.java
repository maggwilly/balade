package balade.data.service.engine.repository;

import balade.data.service.domain.Profile;

import java.util.List;
import java.util.Optional;


import balade.data.service.domain.enumeration.Sex;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Profile entity.
 */
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    List<Profile> findByRegionAndSex(String region, Sex sex);
    List<Profile> findByPlaceAndSex(String place, Sex sex);
    Optional<Profile> findByUserId(Long userId);
    Page<Profile> findByIdIn(List<Long> ids, Pageable pageable);
}
