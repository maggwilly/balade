package balade.data.service.engine;

import balade.data.service.domain.Relation;
import balade.data.service.domain.enumeration.Status;
import balade.data.service.engine.dto.RelationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Relation}.
 */
public interface RelationService {

    /**
     * Save a relation.
     *
     * @param relationDTO the entity to save.
     * @return the persisted entity.
     */
    RelationDTO save(RelationDTO relationDTO);

    /**
     * Get all the relations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RelationDTO> findAll(Pageable pageable);

    /**
     * Get all the relations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RelationDTO> findAllByExpId(Long expId ,Pageable pageable);

    /**
     * Get all the relations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RelationDTO> findAllByExpIdAndStatus(Long expId ,Status status, Pageable pageable);

    /**
     * Get the "id" relation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RelationDTO> findOne(Long id);

    /**
     * Delete the "id" relation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
