package balade.data.service.engine.dto;
import balade.data.service.domain.Picture;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link Picture} entity.
 */
public class PictureDTO implements Serializable {

    private Long id;

    private String path;

    private Long profileId;

    private String blobContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getBlobContentType() {
        return blobContentType;
    }

    public void setBlobContentType(String blobContentType) {
        this.blobContentType = blobContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PictureDTO pictureDTO = (PictureDTO) o;
        if (pictureDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pictureDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PictureDTO{" +
            "id=" + getId() +
            ", path='" + getPath() + "'" +
            ", profileId=" + getProfileId() +
            "}";
    }
}
