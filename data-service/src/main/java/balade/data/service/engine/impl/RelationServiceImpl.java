package balade.data.service.engine.impl;

import balade.data.service.domain.enumeration.Status;
import balade.data.service.engine.RelationService;
import balade.data.service.domain.Relation;
import balade.data.service.engine.repository.RelationRepository;
import balade.data.service.engine.dto.RelationDTO;
import balade.data.service.engine.mapper.RelationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Relation}.
 */
@Service
@Transactional
public class RelationServiceImpl implements RelationService {

    private final Logger log = LoggerFactory.getLogger(RelationServiceImpl.class);

    private final RelationRepository relationRepository;

    private final RelationMapper relationMapper;

    public RelationServiceImpl(RelationRepository relationRepository, RelationMapper relationMapper) {
        this.relationRepository = relationRepository;
        this.relationMapper = relationMapper;
    }

    /**
     * Save a relation.
     *
     * @param relationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RelationDTO save(RelationDTO relationDTO) {
        log.debug("Request to save Relation : {}", relationDTO);
        Relation relation = relationMapper.toEntity(relationDTO);
        relation = relationRepository.save(relation);
        return relationMapper.toDto(relation);
    }

    /**
     * Get all the relations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RelationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Relations");
        return relationRepository.findAll(pageable)
            .map(relationMapper::toDto);
    }

    @Override
    public Page<RelationDTO> findAllByExpId(Long expId, Pageable pageable) {
        log.debug("Request to get all by ExpId Relations");
        return relationRepository.findAllByExpId(expId,pageable)
                .map(relationMapper::toDto);
    }



    @Override
    public Page<RelationDTO> findAllByExpIdAndStatus(Long expId, Status status, Pageable pageable) {
        log.debug("Request to get all by ExpId Relations");
        return relationRepository.findAllByExpIdAndStatus(expId,status.name(),pageable)
                .map(relationMapper::toDto);
    }

    /**
     * Get one relation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RelationDTO> findOne(Long id) {
        log.debug("Request to get Relation : {}", id);
        return relationRepository.findById(id)
            .map(relationMapper::toDto);
    }

    /**
     * Delete the relation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Relation : {}", id);
        relationRepository.deleteById(id);
    }
}
