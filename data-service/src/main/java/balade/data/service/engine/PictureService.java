package balade.data.service.engine;

import balade.data.service.domain.Picture;
import balade.data.service.engine.dto.PictureDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Picture}.
 */
public interface PictureService {

    /**
     * Save a picture.
     *
     * @param pictureDTO the entity to save.
     * @return the persisted entity.
     */
    PictureDTO save(PictureDTO pictureDTO);

    /**
     * Get all the pictures.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PictureDTO> findAll(Pageable pageable);

    /**
     * Get all the pictures by profil id.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PictureDTO> findAllByProfilId(Pageable pageable,Long id);


    /**
     * Get the "id" picture.
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PictureDTO> findOne(Long id);

    /**
     * Delete the "id" picture.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
