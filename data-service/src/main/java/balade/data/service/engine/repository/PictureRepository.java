package balade.data.service.engine.repository;

import balade.data.service.domain.Picture;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;



/**
 * Spring Data  repository for the Picture entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PictureRepository extends JpaRepository<Picture, Long> {

    Page<Picture> findAllByProfileId(Long profileId, Pageable pageable);
}
