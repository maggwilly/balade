package balade.data.service.engine;

import balade.data.service.domain.Profile;
import balade.data.service.domain.enumeration.Sex;
import balade.data.service.engine.dto.ProfileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Profile}.
 */
public interface ProfileService {

    /**
     * Save a profile.
     *
     * @param profileDTO the entity to save.
     * @return the persisted entity.
     */
    ProfileDTO save(ProfileDTO profileDTO);

    /**
     * Get all the profiles.
     *
     * @return the list of entities.
     */
    List<ProfileDTO> findAll();


    /**
     * Get all the profiles by country.
     * @return the list of entities.
     */
     Page<ProfileDTO> findAllByIdIn(List<Long> ids, Pageable pageable);

    /**
     * Get all the profiles by region.
     * @return the list of entities.
     */
    List<ProfileDTO> findAllByRegion(String region, Sex sex);


    /**
     * Get all the profiles by city.
     *
     * @return the list of entities.
     */
    List<ProfileDTO> findAllByPlace(String place, Sex sex);


    /**
     * Get the "id" profile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProfileDTO> findOne(Long id);

    /**
     * Get the "id" profile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProfileDTO> findOneByUserId(Long id);

    /**
     * Delete the "id" profile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
