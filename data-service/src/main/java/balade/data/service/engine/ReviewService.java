package balade.data.service.engine;

import balade.data.service.domain.Review;
import balade.data.service.domain.enumeration.Note;
import balade.data.service.engine.dto.ReviewDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Review}.
 */
public interface ReviewService {

    /**
     * Save a review.
     *
     * @param reviewDTO the entity to save.
     * @return the persisted entity.
     */
    ReviewDTO save(ReviewDTO reviewDTO);

    /**
     * Get all the reviews.
     *
     * @return the list of entities.
     */
    List<ReviewDTO> findAll();

    /**
     * Get all the reviews.
     *
     * @return the list of entities.
     */
    Optional<ReviewDTO> findOneByExpIdAndProfileId(Long expId, Long profileId);

    Optional<Integer> countByNoteAndProfileId(Note note, Long profileId);

    List<ReviewDTO> findAllByProfileId(Long profileId);
    /**
     * Get the "id" review.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ReviewDTO> findOne(Long id);

    /**
     * Delete the "id" review.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
