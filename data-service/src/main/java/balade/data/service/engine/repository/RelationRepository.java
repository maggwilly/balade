package balade.data.service.engine.repository;

import balade.data.service.domain.Relation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;

/**
 * Spring Data  repository for the Relation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RelationRepository extends JpaRepository<Relation, Long> {

    @Query(value ="SELECT * FROM relation r where r.exp_id =:expId OR r.dest_id=:expId", nativeQuery = true)
    Page<Relation> findAllByExpId(@Param("expId") Long expId, Pageable pageable);

    @Query(value ="SELECT * FROM relation r where (r.exp_id =:expId OR r.dest_id =:expId) AND r.status =:status",
            nativeQuery = true)
    Page<Relation> findAllByExpIdAndStatus(@Param("expId") Long expId, @Param("status") String status, Pageable pageable);
}
