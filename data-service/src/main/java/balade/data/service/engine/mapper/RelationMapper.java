package balade.data.service.engine.mapper;

import balade.data.service.domain.Relation;
import balade.data.service.engine.dto.RelationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Relation} and its DTO {@link RelationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RelationMapper extends EntityMapper<RelationDTO, Relation> {



    default Relation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Relation relation = new Relation();
        relation.setId(id);
        return relation;
    }
}
