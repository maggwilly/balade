package balade.data.service.engine.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import balade.data.service.domain.Review;
import balade.data.service.domain.enumeration.Note;

/**
 * A DTO for the {@link Review} entity.
 */
public class ReviewDTO implements Serializable {

    private Long id;

    private ZonedDateTime viewDate;

    private Long userId;

    private Long profileId;

    private Note note;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getViewDate() {
        return viewDate;
    }

    public void setViewDate(ZonedDateTime viewDate) {
        this.viewDate = viewDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReviewDTO reviewDTO = (ReviewDTO) o;
        if (reviewDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reviewDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReviewDTO{" +
            "id=" + getId() +
            ", viewDate='" + getViewDate() + "'" +
            ", userId=" + getUserId() +
            ", profileId=" + getProfileId() +
            ", note='" + getNote() + "'" +
            "}";
    }
}
