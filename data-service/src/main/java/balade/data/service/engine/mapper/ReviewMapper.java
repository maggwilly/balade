package balade.data.service.engine.mapper;

import balade.data.service.domain.Review;
import balade.data.service.engine.dto.ReviewDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Review} and its DTO {@link ReviewDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReviewMapper extends EntityMapper<ReviewDTO, Review> {



    default Review fromId(Long id) {
        if (id == null) {
            return null;
        }
        Review review = new Review();
        review.setId(id);
        return review;
    }
}
