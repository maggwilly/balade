package balade.data.service.engine.impl;

import balade.data.service.domain.enumeration.Sex;
import balade.data.service.engine.ProfileService;
import balade.data.service.domain.Profile;
import balade.data.service.engine.repository.ProfileRepository;
import balade.data.service.engine.dto.ProfileDTO;
import balade.data.service.engine.mapper.ProfileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Profile}.
 */
@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

    private final Logger log = LoggerFactory.getLogger(ProfileServiceImpl.class);

    private final ProfileRepository profileRepository;

    private final ProfileMapper profileMapper;

    public ProfileServiceImpl(ProfileRepository profileRepository, ProfileMapper profileMapper) {
        this.profileRepository = profileRepository;
        this.profileMapper = profileMapper;
    }

    /**
     * Save a profile.
     *
     * @param profileDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProfileDTO save(ProfileDTO profileDTO) {
        log.debug("Request to save Profile : {}", profileDTO);
        Profile profile = profileMapper.toEntity(profileDTO);
        profile = profileRepository.save(profile);
        return profileMapper.toDto(profile);
    }

    /**
     * Get all the profiles.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProfileDTO> findAll() {
        log.debug("Request to get all Profiles");
        return profileRepository.findAll().stream()
            .map(profileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Page<ProfileDTO> findAllByIdIn(List<Long> ids, Pageable pageable) {
        return profileRepository.findByIdIn(ids,pageable).map(profileMapper::toDto);
    }

    @Override
    public List<ProfileDTO> findAllByRegion(String region, Sex sex) {
        return profileRepository.findByRegionAndSex(region,sex).stream()
                .map(profileMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<ProfileDTO> findAllByPlace(String place, Sex sex) {
        return profileRepository.findByPlaceAndSex(place,sex).stream()
                .map(profileMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<ProfileDTO> findOneByUserId(Long id) {
        return profileRepository.findByUserId(id)
                .map(profileMapper::toDto);
    }

    /**
     * Get one profile by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProfileDTO> findOne(Long id) {
        log.debug("Request to get Profile : {}", id);
        return profileRepository.findById(id)
            .map(profileMapper::toDto);
    }

    /**
     * Delete the profile by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Profile : {}", id);
        profileRepository.deleteById(id);
    }
}
