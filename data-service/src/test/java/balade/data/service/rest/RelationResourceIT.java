package balade.data.service.rest;

import balade.data.service.DataServiceApplication;
import balade.data.service.domain.Relation;
import balade.data.service.engine.repository.RelationRepository;
import balade.data.service.engine.RelationService;
import balade.data.service.engine.dto.RelationDTO;
import balade.data.service.engine.mapper.RelationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static balade.data.service.rest.TestUtil.sameInstant;
import static balade.data.service.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import balade.data.service.domain.enumeration.Status;
/**
 * Integration tests for the {@link RelationResource} REST controller.
 */
@SpringBootTest(classes = DataServiceApplication.class)
public class RelationResourceIT {

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_EXP_ID = 1L;
    private static final Long UPDATED_EXP_ID = 2L;

    private static final Long DEFAULT_DEST_ID = 1L;
    private static final Long UPDATED_DEST_ID = 2L;

    private static final Status DEFAULT_STATUS = Status.ACCEPTED;
    private static final Status UPDATED_STATUS = Status.REFUSED;

    @Autowired
    private RelationRepository relationRepository;

    @Autowired
    private RelationMapper relationMapper;

    @Autowired
    private RelationService relationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private Environment env;
    @Autowired
    private CustomExceptionHandler customExceptionHandler;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRelationMockMvc;

    private Relation relation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RelationResource relationResource = new RelationResource(relationService,env);
        this.restRelationMockMvc = MockMvcBuilders.standaloneSetup(relationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(customExceptionHandler)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Relation createEntity(EntityManager em) {
        Relation relation = new Relation()
            .createdDate(DEFAULT_CREATED_DATE)
            .expId(DEFAULT_EXP_ID)
            .destId(DEFAULT_DEST_ID)
            .status(DEFAULT_STATUS);
        return relation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Relation createUpdatedEntity(EntityManager em) {
        Relation relation = new Relation()
            .createdDate(UPDATED_CREATED_DATE)
            .expId(UPDATED_EXP_ID)
            .destId(UPDATED_DEST_ID)
            .status(UPDATED_STATUS);
        return relation;
    }

    @BeforeEach
    public void initTest() {
        relation = createEntity(em);
    }

    @Test
    @Transactional
    public void createRelation() throws Exception {
        int databaseSizeBeforeCreate = relationRepository.findAll().size();

        // Create the Relation
        RelationDTO relationDTO = relationMapper.toDto(relation);
        restRelationMockMvc.perform(post("/api/relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relationDTO)))
            .andExpect(status().isCreated());

        // Validate the Relation in the database
        List<Relation> relationList = relationRepository.findAll();
        assertThat(relationList).hasSize(databaseSizeBeforeCreate + 1);
        Relation testRelation = relationList.get(relationList.size() - 1);
        assertThat(testRelation.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testRelation.getExpId()).isEqualTo(DEFAULT_EXP_ID);
        assertThat(testRelation.getDestId()).isEqualTo(DEFAULT_DEST_ID);
        assertThat(testRelation.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createRelationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = relationRepository.findAll().size();

        // Create the Relation with an existing ID
        relation.setId(1L);
        RelationDTO relationDTO = relationMapper.toDto(relation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRelationMockMvc.perform(post("/api/relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Relation in the database
        List<Relation> relationList = relationRepository.findAll();
        assertThat(relationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRelations() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        // Get all the relationList
        restRelationMockMvc.perform(get("/api/relations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[*].id").value(hasItem(relation.getId().intValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].expId").value(hasItem(DEFAULT_EXP_ID.intValue())))
            .andExpect(jsonPath("$.[*].destId").value(hasItem(DEFAULT_DEST_ID.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void findAllByExpId() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        // Get all the relationList
        restRelationMockMvc.perform(get("/api/relations/by/expId/{id}",relation.getExpId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(relation.getId().intValue())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
                .andExpect(jsonPath("$.[*].expId").value(hasItem(DEFAULT_EXP_ID.intValue())))
                .andExpect(jsonPath("$.[*].destId").value(hasItem(DEFAULT_DEST_ID.intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void findAllByExpIdAndStatus() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        // Get all the relationList
        restRelationMockMvc.perform(get("/api/relations/by/expId/{id}/status/{status}",relation.getExpId(),relation.getStatus()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(relation.getId().intValue())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
                .andExpect(jsonPath("$.[*].expId").value(hasItem(DEFAULT_EXP_ID.intValue())))
                .andExpect(jsonPath("$.[*].destId").value(hasItem(DEFAULT_DEST_ID.intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getRelation() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        // Get the relation
        restRelationMockMvc.perform(get("/api/relations/{id}", relation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(relation.getId().intValue()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.expId").value(DEFAULT_EXP_ID.intValue()))
            .andExpect(jsonPath("$.destId").value(DEFAULT_DEST_ID.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }



    @Test
    @Transactional
    public void getNonExistingRelation() throws Exception {
        // Get the relation
        restRelationMockMvc.perform(get("/api/relations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRelation() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        int databaseSizeBeforeUpdate = relationRepository.findAll().size();

        // Update the relation
        Relation updatedRelation = relationRepository.findById(relation.getId()).get();
        // Disconnect from session so that the updates on updatedRelation are not directly saved in db
        em.detach(updatedRelation);
        updatedRelation
            .createdDate(UPDATED_CREATED_DATE)
            .expId(UPDATED_EXP_ID)
            .destId(UPDATED_DEST_ID)
            .status(UPDATED_STATUS);
        RelationDTO relationDTO = relationMapper.toDto(updatedRelation);

        restRelationMockMvc.perform(put("/api/relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relationDTO)))
            .andExpect(status().isOk());

        // Validate the Relation in the database
        List<Relation> relationList = relationRepository.findAll();
        assertThat(relationList).hasSize(databaseSizeBeforeUpdate);
        Relation testRelation = relationList.get(relationList.size() - 1);
        assertThat(testRelation.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testRelation.getExpId()).isEqualTo(UPDATED_EXP_ID);
        assertThat(testRelation.getDestId()).isEqualTo(UPDATED_DEST_ID);
        assertThat(testRelation.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingRelation() throws Exception {
        int databaseSizeBeforeUpdate = relationRepository.findAll().size();

        // Create the Relation
        RelationDTO relationDTO = relationMapper.toDto(relation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRelationMockMvc.perform(put("/api/relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(relationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Relation in the database
        List<Relation> relationList = relationRepository.findAll();
        assertThat(relationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRelation() throws Exception {
        // Initialize the database
        relationRepository.saveAndFlush(relation);

        int databaseSizeBeforeDelete = relationRepository.findAll().size();

        // Delete the relation
        restRelationMockMvc.perform(delete("/api/relations/{id}", relation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Relation> relationList = relationRepository.findAll();
        assertThat(relationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
