package balade.msg.service;

import balade.msg.service.domain.Message;
import balade.msg.service.domain.Signal;
import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.MessageDataService;
import balade.msg.service.service.RoomDataService;
import balade.msg.service.service.UserIdDataService;
import balade.msg.service.service.listener.*;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class WebRTCconfig {

    @Autowired
    public IClientManager clientManager;

    @Autowired
    public UserIdDataService userIdDataService;

    @Autowired
    public RoomDataService roomIDataService;

    @Autowired
    public MessageDataService messageIDataService;
    @Bean
    public ConnectListener connectListener(){
        return new ConnexionListener();
    }


    @Bean
    public DataListener<UserId> userIdDataListener(){
        return new UserIdListener(clientManager, userIdDataService);
    }

    @Bean
    public DataListener<Signal> signalDataListener(){
        return new SignalListener(clientManager, messageIDataService, userIdDataService,roomIDataService);
    }

    @Bean
    public DataListener<Message> messageDataListener(){
        return new MessageListener(clientManager, roomIDataService, messageIDataService);
    }
    @Bean
    public DisconnectListener disconnectListener(){
        return new DisConnexionListener(clientManager,userIdDataService);
    }

    @Bean
    public DataListener<String>  echoListener(){
        return (client, data, ackSender) -> {
            client.sendEvent("echoBack", data);
        };
    }
}
