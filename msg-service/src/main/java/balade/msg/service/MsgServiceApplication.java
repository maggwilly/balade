package balade.msg.service;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;


@SpringBootApplication
public class MsgServiceApplication {

    @Autowired
    private Environment env;
	@Bean
	public SocketIOServer socketIOServer() {
		return new SocketIOServer(rtcConfig ());
	}

	public static void main(String[] args) {
		SpringApplication.run(MsgServiceApplication.class, args);
	}

	@Bean
	public Configuration rtcConfig (){
		Configuration config = new Configuration();
		config.setHostname(env.getProperty("rt-server.host"));
		config.setPort(Integer.parseInt(env.getProperty("rt-server.port")));
		return config;
	}
}
