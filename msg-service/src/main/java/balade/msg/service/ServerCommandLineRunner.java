package balade.msg.service;

import balade.msg.service.domain.Message;
import balade.msg.service.domain.Signal;
import balade.msg.service.domain.UserId;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ServerCommandLineRunner implements CommandLineRunner {
    private final Logger log = LoggerFactory.getLogger(ServerCommandLineRunner.class);
    @Autowired
    private  SocketIOServer server;
    @Autowired
    private ConnectListener connectListener;
    @Autowired
    private DataListener<String>  echoListener;
    @Autowired
    private DisconnectListener disconnectListener;
    @Autowired
    private DataListener<Signal> signalDataListener;
    @Autowired
    private DataListener<Message> messageDataListener;
    @Autowired
    private DataListener<UserId> userIdDataListener;


    @Override
    public void run(String... args) throws Exception {
        this.server.addConnectListener(connectListener);
        this.server.addDisconnectListener(disconnectListener);
        this.server.addEventListener("echo", String.class, echoListener);
        this.server.addEventListener("userId", UserId.class, userIdDataListener);
        this.server.addEventListener("signal", Signal.class, signalDataListener);
        this.server.addEventListener("message", Message.class, messageDataListener);
        server.start();
    }



}
