package balade.msg.service.service.impl;

import balade.msg.service.domain.*;
import balade.msg.service.repository.mongo.UserIdRepository;
import balade.msg.service.service.UserIdDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserIdService implements UserIdDataService {
    final Logger logger = LoggerFactory.getLogger(UserIdService.class);

    @Autowired
    private UserIdRepository dataRepository;

    @Override
    public void saveUserId(UserId data) {
        logger.debug("Save data Class [{}] - Id '{}'", UserId.class.getCanonicalName(), data.getId());
        dataRepository.save(data);
    }

    @Override
    public Optional<UserId> findUserIdById(String key) {
        return   dataRepository.findById(key);
    }

    @Override
    public Optional<UserId> findUserIdBySessionId(String key) {
        return  dataRepository.findBySessionId(key);
    }

    @Override
    public void deleteUserId(String key) {
        dataRepository.deleteById(key);
    }

    @Override
    public List<UserId> findUserFriends(String id) {
        UserId userId= findUserIdById(id).get();
        return  dataRepository.findByUserIds(userId.getUserIds());
    }
}
