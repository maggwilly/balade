package balade.msg.service.service.listener;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnexionListener implements ConnectListener {
    private final Logger log = LoggerFactory.getLogger(ConnexionListener.class);

    @Override
    public void onConnect(SocketIOClient client) {
    }
}
