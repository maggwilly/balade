package balade.msg.service.service;


import com.corundumstudio.socketio.SocketIOClient;

public interface IClientManager {
    SocketIOClient getClient(String sessionId);
    Boolean isNotNull(Object object);
}
