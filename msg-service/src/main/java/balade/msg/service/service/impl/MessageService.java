package balade.msg.service.service.impl;

import balade.msg.service.domain.Message;
import balade.msg.service.domain.UserId;
import balade.msg.service.repository.mongo.MessageRepository;
import balade.msg.service.service.MessageDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class MessageService implements MessageDataService {
    final Logger logger = LoggerFactory.getLogger(MessageService.class);

    @Autowired
    private MessageRepository dataRepository;

    @Override
    public String saveMessage(Message data) {
        logger.debug("Save data Class [{}] - Id '{}'", UserId.class.getCanonicalName(), data.getId());
       return dataRepository.save(data).getId();
    }

    @Override
    public Optional<Message> findMessageById(String key) {
        return   dataRepository.findById(key);
    }

    @Override
    public void deleteMessage(String key) {
        dataRepository.deleteById(key);
    }

    @Override
    public List<Message> findMessageByRoomId(String id) {
        return dataRepository.findByRoomId(id);
    }
}
