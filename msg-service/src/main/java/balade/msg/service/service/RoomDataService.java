package balade.msg.service.service;

import balade.msg.service.domain.Room;
import balade.msg.service.domain.UserId;

import java.util.List;
import java.util.Optional;

public interface RoomDataService {
    String saveRoom(Room data);
    public Optional<Room> findRoomById(String key);
    public List<Room> findRoomByUserId(String id);
    public List<Room> findRoomBy2UserIds(String exp, String dest);

    public List<UserId> findByRoomMembers(String id);
    void deleteRoom(String key);
}
