package balade.msg.service.service;

import balade.msg.service.domain.Message;

import java.util.List;
import java.util.Optional;

public interface MessageDataService {
    String saveMessage(Message data);
    public Optional<Message> findMessageById(String key);
    public List<Message> findMessageByRoomId(String id);
    void deleteMessage(String key);
}
