package balade.msg.service.service.listener;


import balade.msg.service.domain.ennum.DataType;
import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.UserIdDataService;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

public class UserIdListener implements DataListener<UserId> {
    private final Logger log = LoggerFactory.getLogger(UserIdListener.class);
    private IClientManager clientManager;
    private final UserIdDataService userIdIDataService;
    private  final String ACK_EVENT_NAME="ack";
    public UserIdListener(IClientManager clientManager, UserIdDataService dataService) {
        this.clientManager = clientManager;
        this.userIdIDataService = dataService;
    }

    @Override
    public void onData(SocketIOClient client, UserId data, AckRequest ackSender) {
        data.setOnline(true);
        data.setLastDate(System.currentTimeMillis());
        data.setSessionId(client.getSessionId().toString());
        userIdIDataService.saveUserId(data);
        List<UserId> friends =userIdIDataService.findUserFriends(data.getId());
        friends.forEach( next ->{
            if (clientManager.isNotNull(next)){
                String sessionId=next.getSessionId();
                if (clientManager.isNotNull(sessionId)){
                   SocketIOClient cli=  clientManager.getClient(sessionId);
                   if(clientManager.isNotNull(cli))
                      cli.sendEvent(DataType.online.name(),data);}
             }
        });
        client.sendEvent(ACK_EVENT_NAME,"done");
    }
}
