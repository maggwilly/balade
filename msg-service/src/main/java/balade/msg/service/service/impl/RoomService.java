package balade.msg.service.service.impl;


import balade.msg.service.domain.*;
import balade.msg.service.repository.mongo.MessageRepository;
import balade.msg.service.repository.mongo.RoomRepository;
import balade.msg.service.repository.mongo.UserIdRepository;
import balade.msg.service.service.RoomDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class RoomService implements RoomDataService {
    final Logger logger = LoggerFactory.getLogger(RoomService.class);

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserIdRepository dataUserRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public String saveRoom(Room data) {
        logger.debug("Save data Class [{}] - Id '{}'", UserId.class.getCanonicalName(), data.getId());
        return roomRepository.save(data).getId();
    }

    @Override
    public Optional<Room> findRoomById(String key) {
        return   roomRepository.findById(key);
    }

    @Override
    public void deleteRoom(String key) {
        List<Message> messages= messageRepository.findByRoomId(key);
          messages.forEach(message -> messageRepository.deleteById(message.getId()));
        roomRepository.deleteById(key);
    }

    @Override
    public List<Room> findRoomByUserId(String id) {
        return roomRepository.findByUserId(id);
    }

    @Override
    public List<Room> findRoomBy2UserIds(String exp, String dest) {
        return roomRepository.findBy2UserIds(new String[]{exp,dest});
    }

    @Override
    public List<UserId> findByRoomMembers(String id) {
         Room room= findRoomById(id).get();
        return  dataUserRepository.findByUserIds(room.getMembers());
    }


}
