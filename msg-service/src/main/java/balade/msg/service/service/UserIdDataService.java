package balade.msg.service.service;

import balade.msg.service.domain.UserId;

import java.util.List;
import java.util.Optional;

public interface UserIdDataService {
    void saveUserId(UserId data);
    public Optional<UserId> findUserIdById(String key);
    public Optional<UserId> findUserIdBySessionId(String key);
    public List<UserId> findUserFriends(String id);
    void deleteUserId(String key);
}
