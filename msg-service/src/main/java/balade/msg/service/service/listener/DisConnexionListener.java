package balade.msg.service.service.listener;

import balade.msg.service.domain.ennum.DataType;
import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.UserIdDataService;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DisconnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

public class DisConnexionListener implements DisconnectListener {
    private final Logger log = LoggerFactory.getLogger(DisConnexionListener.class);
    private final UserIdDataService userIdIDataService;
    private IClientManager clientManager;
    public DisConnexionListener(IClientManager clientManager, UserIdDataService dataService) {
        this.userIdIDataService = dataService;
        this.clientManager = clientManager;

    }

    @Override
    public void onDisconnect(SocketIOClient client) {
        UserId usr = userIdIDataService.findUserIdBySessionId(client.getSessionId().toString()).get();
        if (clientManager.isNotNull(usr)) {
            usr.setOnline(false);
            userIdIDataService.saveUserId(usr);
            List<UserId> friends = userIdIDataService.findUserFriends(usr.getId());
            friends.forEach (next->{
                if (clientManager.isNotNull(next)) {
                    String sessionId=next.getSessionId();
                    if (clientManager.isNotNull(sessionId)){
                        SocketIOClient cli=  clientManager.getClient(sessionId);
                        if(clientManager.isNotNull(cli))
                            cli.sendEvent(DataType.offline.name(),usr);}
                }
            });
        }
    }
}
