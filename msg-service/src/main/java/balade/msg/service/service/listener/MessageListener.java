package balade.msg.service.service.listener;


import balade.msg.service.domain.*;
import balade.msg.service.domain.ennum.DataType;
import balade.msg.service.service.*;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

public class MessageListener implements DataListener<Message> {
    private final Logger log = LoggerFactory.getLogger(MessageListener.class);
    private IClientManager clientManager;
    private final RoomDataService roomIDataService;
    private final MessageDataService messageIDataService;
    private  final String POST_EVENT_NAME="post";
    private  final String SENT_EVENT_NAME="sent";
    public MessageListener(IClientManager clientManager, RoomDataService dataService,  MessageDataService messageIDataService) {
        this.clientManager = clientManager;
        this.roomIDataService = dataService;
        this.messageIDataService = messageIDataService;
    }

    @Override
    public void onData(SocketIOClient client, Message data, AckRequest ackSender) {
               data.setRoomId(room(data));
               String messageId= messageIDataService.saveMessage(data);
               List<UserId> members=roomIDataService.findByRoomMembers(data.getRoomId());
                 members.forEach(dest -> {
                   if(clientManager.isNotNull(dest)&&dest.getId()!=data.getExpId()){
                       SocketIOClient destCli = clientManager.getClient(dest.getSessionId());
                       if (clientManager.isNotNull(destCli)){
                           destCli.sendEvent(DataType.new_message.name(), data);
                           client.sendEvent(SENT_EVENT_NAME, messageId);
                       }
                   }
               });
              client.sendEvent(POST_EVENT_NAME, messageId);
       }


     private  String room(Message message){
        Room room=  roomIDataService.findRoomById(message.getRoomId()).get();
        if(room==null){
            List<Room> rooms =roomIDataService.findRoomBy2UserIds(message.getExpId(), message.getDestId());
            if(!rooms.isEmpty())
                return rooms.get(0).getId();
                room = new Room(message.getRoomId());
            room.getMembers().add(message.getExpId());
            room.getMembers().add(message.getDestId());
            room.setId(roomIDataService.saveRoom(room));
         }
         return room.getId();
     }

    }

