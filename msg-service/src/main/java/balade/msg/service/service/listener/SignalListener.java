package balade.msg.service.service.listener;

import balade.msg.service.domain.Signal;
import balade.msg.service.domain.UserId;
import balade.msg.service.service.*;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.DataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SignalListener implements DataListener<Signal> {
    private final Logger log = LoggerFactory.getLogger(SignalListener.class);
    private IClientManager clientManager;
    private final MessageDataService messageDataService;
    private final RoomDataService roomIDataService;
    private final UserIdDataService userIdIDataService;
    private  final String ACK_EVENT_NAME="ack";
    public SignalListener(IClientManager clientManager, MessageDataService messageDataService, UserIdDataService dataService, RoomDataService roomIDataService) {
        this.clientManager = clientManager;
        this.messageDataService = messageDataService;
        this.userIdIDataService = dataService;
        this.roomIDataService = roomIDataService;
    }

    @Override
    public void onData(SocketIOClient client, Signal data, AckRequest ackSender) {
        log.debug("Client[{}] - Received chat message '{}'", client.getSessionId().toString(), data);
        switch (data.getDataType()) {
            case roomDelete:
            case lastMessageReaded:
                roomIDataService.deleteRoom(data.getRoomId());
                client.sendEvent(ACK_EVENT_NAME, "done");
                break;
            case messages:
                client.sendEvent(data.getId(), messageDataService.findMessageByRoomId(data.getRoomId()));
                break;
            case friends:
                client.sendEvent(data.getId(),userIdIDataService.findUserFriends(data.getExpId()));
                break;
            case rooms:
                client.sendEvent(data.getId(),roomIDataService.findRoomByUserId(data.getExpId()));
              break;
            default:
                UserId dest = userIdIDataService.findUserIdById(data.getDestId()).get();
                if(clientManager.isNotNull(dest)) {
                    SocketIOClient destCli = clientManager.getClient(dest.getSessionId());
                    if (clientManager.isNotNull(destCli)) {
                        destCli.sendEvent(data.getDataType().name(), data);
                        client.sendEvent(ACK_EVENT_NAME, "done");
                    }
                }
                break;
         }
     }
}