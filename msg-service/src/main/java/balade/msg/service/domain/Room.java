package balade.msg.service.domain;

import org.springframework.data.annotation.Id;
import java.util.ArrayList;
import java.util.List;


public class Room extends  Data {
    @Id
     private  String id;
     private  List<String> members =new ArrayList<>();
     private  Message lastMessage;
     private  int newMessages;
    public Room() {
    }
    public Room(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getNewMessages() {
        return newMessages;
    }

    public void setNewMessages(int newMessages) {
        this.newMessages = newMessages;
    }
}
