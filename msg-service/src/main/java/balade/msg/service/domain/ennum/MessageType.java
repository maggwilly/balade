package balade.msg.service.domain.ennum;

public enum MessageType {
    TEXT,
    VIDEO,
    IMAGE
}
