package balade.msg.service.domain.ennum;

public enum DataType {
    new_message,
    friends,
    writting,
    endWritting,
    lastMessageReaded,
    roomDelete,
    messages,
    rooms,
    offline,
    online;

}
