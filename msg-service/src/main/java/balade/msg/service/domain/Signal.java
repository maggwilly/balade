package balade.msg.service.domain;

public class Signal extends  Data {
     private  String id;
     private  String expId;
     private  String destId;
     private  String roomId;
    @Override
    public String getId() {
        return id;
    }

    public String getExpId() {
        return expId;
    }

    public void setExpId(String expId) {
        this.expId = expId;
    }

    public String getDestId() {
        return destId;
    }

    public void setDestId(String destId) {
        this.destId = destId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setId(String id) {
        this.id = id;
    }
}
