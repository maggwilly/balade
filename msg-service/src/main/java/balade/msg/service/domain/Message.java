package balade.msg.service.domain;

import balade.msg.service.domain.ennum.MessageType;
import org.springframework.data.annotation.Id;

public class Message extends  Data {
    @Id
    private  String id;
     private  String expId;
     private  String destId;
     private  String content;
     private  String type;
     private  String roomId;
     private MessageType msgType;
     private  long time;
    @Override
    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpId() {
        return expId;
    }

    public void setExpId(String expId) {
        this.expId = expId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public MessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(MessageType msgType) {
        this.msgType = msgType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDestId() {
        return destId;
    }

    public void setDestId(String destId) {
        this.destId = destId;
    }

}
