package balade.msg.service.domain;

import balade.msg.service.domain.ennum.DataType;

import java.io.Serializable;

public abstract class Data implements  Serializable {
    private DataType dataType;
    public abstract String getId();
    public void setDataType(DataType dataType) {
        this.dataType=dataType;
    }

    public  DataType getDataType() {
        return dataType;
    }

}
