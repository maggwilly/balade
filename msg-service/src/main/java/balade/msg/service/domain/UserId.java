package balade.msg.service.domain;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class UserId extends  Data {
    @Id
    private  String userId;
    private  String sessionId;
    private  List<String> userIds =new ArrayList<>();
    private boolean online;
    private long lastDate;
    public UserId() {
    }
    public UserId(String userId) {
        this.userId = userId;
    }

    public UserId(String userId, String sessionId, boolean online) {
        this.userId = userId;
        this.sessionId = sessionId;
        this.online = online;
    }

    @Override
    public String getId() {
        return userId ;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public long getLastDate() {
        return lastDate;
    }

    public void setLastDate(long lastDate) {
        this.lastDate = lastDate;
    }
}
