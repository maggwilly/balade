package balade.msg.service.repository.mongo;

import balade.msg.service.domain.Message;;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MessageRepository extends MongoRepository<Message, String>{
    public List<Message> findByRoomId(String rooId);
}
