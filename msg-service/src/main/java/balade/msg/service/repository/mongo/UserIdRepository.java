package balade.msg.service.repository.mongo;

import balade.msg.service.domain.UserId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserIdRepository extends MongoRepository<UserId, String>{
    @Query("{ userIds: {$in : ?0}}")
    public List<UserId> findByUserIds(List<String> ids);

    public Optional<UserId> findBySessionId(String sessionId);
}

