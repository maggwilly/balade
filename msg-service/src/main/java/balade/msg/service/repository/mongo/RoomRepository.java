package balade.msg.service.repository.mongo;

import balade.msg.service.domain.Room;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import java.util.List;

public interface RoomRepository extends MongoRepository<Room, String>{

    @Query("{members : {$in : ?0}}")
     public List<Room> findByUserId(String id);

     @Query("{members : {$in : ?0}}")
     public List<Room> findBy2UserIds(String[] ids);
}
