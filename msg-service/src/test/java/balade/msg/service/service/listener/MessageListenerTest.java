package balade.msg.service.service.listener;

import balade.msg.service.domain.Room;
import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.MessageDataService;
import balade.msg.service.service.RoomDataService;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "/test.application.properties")
class MessageListenerTest {
    final int  TIMEOUT=7000;

    static private Socket socket;
    @MockBean
    private DisconnectListener disconnectListener;
    @MockBean
    public MessageDataService messageIDataService;
    @MockBean
    public RoomDataService roomIDataService;
    @MockBean
    private ConnectListener connectListener;
    @MockBean
    private IClientManager clientManager;
    private  final String MESSAGE_EVENT_NAME="message";
    private  final String  POST_EVENT_NAME="post";
    private  final String SENT_EVENT_NAME="sent";

    private UserId array[] = {
            new UserId("1", UUID.randomUUID().toString(), false),
            new UserId("2",UUID.randomUUID().toString(),true),
            new UserId("3",UUID.randomUUID().toString(),false)};


    @Autowired
    public Configuration rtcConfig;


    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }


    @Timeout(TIMEOUT)
    @DisplayName("sentMessage")
    @Test
    void sentMessage() throws JSONException, InterruptedException {
        SocketIOClient client = spy(SocketIOClient.class);
        doNothing().when(disconnectListener).onDisconnect(any());
        doNothing().when(connectListener).onConnect(any());
        when(messageIDataService.saveMessage(any())).thenReturn("msg");
        when(roomIDataService.saveRoom(any())).thenReturn("room1");
        when(roomIDataService.findByRoomMembers(any())).thenReturn(Arrays.asList(array));
        when(roomIDataService.findRoomById(any())).thenReturn(Optional.of(new Room()));
        when(clientManager.getClient(any())).thenReturn(client);
        when(clientManager.isNotNull(any())).thenReturn(true);
        JSONObject obj =TestUtil.createMessage();
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(POST_EVENT_NAME, args ->{
                values.offer("done");
            } );
            socket.emit(MESSAGE_EVENT_NAME,obj);
        });
        socket.connect();
        values.take();
        verify(client,times(3)).sendEvent(anyString(),any());
    }

    @AfterEach
    void after() {
        socket.disconnect();
    }

}