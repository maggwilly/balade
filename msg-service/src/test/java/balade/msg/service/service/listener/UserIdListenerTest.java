package balade.msg.service.service.listener;

import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.UserIdDataService;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(locations = "/test.application.properties")
class UserIdListenerTest {
    final int  TIMEOUT=7000;
    static private Socket socket;

    private final  String USER_STATUS_EVENT="userId";
    @MockBean
    private DisconnectListener disconnectListener;
    @MockBean
    private IClientManager clientManager;
    @MockBean
    private ConnectListener connectListener;
    @MockBean
    private UserIdDataService dataService;
    private  final String ACK_EVENT_NAME="ack";

    private UserId array[] = {
            new UserId("1", UUID.randomUUID().toString(), false),
            new UserId("2",UUID.randomUUID().toString(),false),
            new UserId("3",UUID.randomUUID().toString(),false)};


    @Autowired
    public Configuration rtcConfig;


    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }


    @Timeout(TIMEOUT)
    @DisplayName("onUserIdSent")
    @Test
    void onUserIdSentToServer() throws InterruptedException, JSONException {
        doNothing().when(disconnectListener).onDisconnect(any());
        doNothing().when(connectListener).onConnect(any());
        SocketIOClient client = spy(SocketIOClient.class);
        UserId userId=new UserId("dest1", UUID.randomUUID().toString(),true);
        when(dataService.findUserIdById(anyString())).thenReturn(Optional.of(userId));
        doNothing().when(dataService).saveUserId(any());
        doNothing().when(client).sendEvent(any(), any());
        when(dataService.findUserFriends(anyString())).thenReturn(Arrays.asList(array));
        when(clientManager.getClient(any())).thenReturn(client);
        when(clientManager.isNotNull(any())).thenReturn(true);
        JSONObject obj =TestUtil.createUserId(userId.getId());
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(ACK_EVENT_NAME, args ->{
                values.offer("done");
            } );
            socket.emit(USER_STATUS_EVENT,obj);
        });
        socket.connect();
        values.take();
        verify(client,times(3)).sendEvent(anyString(),any());
    }

    @AfterEach
    void tearDown() {
        socket.disconnect();
    }
}