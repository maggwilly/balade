package balade.msg.service.service.listener;

import balade.msg.service.domain.ennum.DataType;
import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URISyntaxException;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.net.URISyntaxException;
import java.util.UUID;
import java.util.logging.Logger;

public abstract class TestUtil {

    private static final Logger logger = Logger.getLogger(TestUtil.class.getName());

    final static int TIMEOUT = 7000;



    static  Socket client() throws URISyntaxException {
        return client(createOptions());
    }

    static Socket client(String path) throws URISyntaxException {
        return client(path, createOptions());
    }

    static Socket client(IO.Options opts) throws URISyntaxException {
        return client(nsp(), opts);
    }

    static Socket client(String path, IO.Options opts) throws URISyntaxException {
        return IO.socket(uri(opts.port) + path, opts);
    }

    static String uri(int port) {
        return "http://localhost:"+port;
    }

    static String nsp() {
        return "/";
    }

    static IO.Options createOptions() {
        IO.Options opts = new IO.Options();
        opts.port=9092;
        opts.forceNew = true;
        return opts;
    }
    static JSONObject createSignal(DataType...types) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("id", UUID.randomUUID().toString());
        obj.put("roomId", UUID.randomUUID().toString());
        obj.put("roomId", UUID.randomUUID().toString());
        obj.put("destId", UUID.randomUUID().toString());
        if(types.length>0)
            obj.put("dataType", types[0].name());
        obj.put("expId", UUID.randomUUID().toString());
        return obj;
    }
    static JSONObject createMessage() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("roomId",  UUID.randomUUID().toString());
        obj.put("expId",  UUID.randomUUID().toString());
        return obj;
    }

    public static JSONObject createUserId(String id) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("userIds",new JSONArray(new String[]{"dest1","dest2","dest3"}));
        obj.put("userId", id);
        return obj;
    }

}
