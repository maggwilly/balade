package balade.msg.service.service.listener;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@SpringBootTest
@TestPropertySource(locations = "/test.application.properties")
class ConnexionListenerTest {
   final int  TIMEOUT=7000;
    static private Socket socket;
    private  final String ECHO_EVENT_NAME="echo";
    private  final String ECHO_BACK_EVENT_NAME="echoBack";
    @MockBean
    private DisconnectListener disconnectListener;
    @Autowired
    public Configuration rtcConfig;


    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }

    @Timeout(TIMEOUT)
    @DisplayName("onConnectToLocalhost")
    @Test
    public void onConnectToLocalhost() throws URISyntaxException, InterruptedException {
        doNothing().when(disconnectListener).onDisconnect(any());
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(ECHO_BACK_EVENT_NAME, args -> values.offer("done"));
            socket.emit(ECHO_EVENT_NAME,"Eco");
        });
        socket.connect();
        values.take();
        socket.close();
    }

    @Timeout(TIMEOUT)
    @DisplayName("Test can receive data from server")
    @Test
    public void receiveUTF8MultibyteCharacters() throws URISyntaxException, InterruptedException {
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        final String[] correct = new String[] {
                "てすと",
                "Я Б Г Д Ж Й",
                "Ä ä Ü ü ß",
                "utf8 — string",
                "utf8 — string"
        };

        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(ECHO_BACK_EVENT_NAME, args -> values.offer(args[0]));
            for (String data : correct) {
                socket.emit(ECHO_EVENT_NAME, data);
            }
        });
        socket.connect();
        for (String expected : correct) {
            assertEquals(values.take(), expected);
        }
        socket.close();
    }

    @AfterEach
    void after() {
        socket.disconnect();
    }

}