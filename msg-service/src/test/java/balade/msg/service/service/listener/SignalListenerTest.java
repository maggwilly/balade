package balade.msg.service.service.listener;

import balade.msg.service.domain.ennum.DataType;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.MessageDataService;
import balade.msg.service.service.RoomDataService;
import balade.msg.service.service.UserIdDataService;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

@SpringBootTest
@TestPropertySource(locations = "/test.application.properties")
class SignalListenerTest {
    final int  TIMEOUT=7000;
     private Socket socket;

    private final  String SIGNAL_SENT_EVENT="signal";
    @MockBean
    private DisconnectListener disconnectListener;

    @MockBean
    private IClientManager clientManager;
    @MockBean
    private ConnectListener connectListener;
    @MockBean
    private UserIdDataService dataService;
    @MockBean
    private  RoomDataService roomIDataService;
    @MockBean
    private  MessageDataService messageIDataService;
    private  final String ACK_EVENT_NAME="ack";
    @Autowired
    public Configuration rtcConfig;


    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }

    @Timeout(TIMEOUT)
    @DisplayName("requestForDeleteRoom")
    @Test
   void requestForDeleteRoom() throws JSONException, InterruptedException {
        doNothing().when(disconnectListener).onDisconnect(any());
        doNothing().when(connectListener).onConnect(any());
       doNothing().when(roomIDataService).deleteRoom(any());
        JSONObject obj =TestUtil.createSignal(DataType.roomDelete);
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(ACK_EVENT_NAME, args ->{
                values.offer(args[0]);
            } );
            socket.emit(SIGNAL_SENT_EVENT,obj);
        });
        socket.connect();
        assertEquals(values.take(), "done");
        verify(roomIDataService).deleteRoom(obj.getString("roomId"));

    }

    @AfterEach
    void tearDown() {
        socket.disconnect();
    }
}