package balade.msg.service.service.listener;

import balade.msg.service.domain.UserId;
import balade.msg.service.service.IClientManager;
import balade.msg.service.service.UserIdDataService;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(locations = "/test.application.properties")
class DisConnexionListenerTest {
    final int  TIMEOUT=7000;
    static private Socket socket;

    @MockBean
    private IClientManager clientManager;
    @MockBean
    private ConnectListener connectListener;
    @MockBean
    private UserIdDataService dataService;
    @Autowired
    public Configuration rtcConfig;


    @BeforeEach
    void setUp() throws URISyntaxException {
        IO.Options opts = new IO.Options();
        opts.port=rtcConfig.getPort();
        socket = TestUtil.client(opts);
    }

    UserId array[] = {
            new UserId("1",UUID.randomUUID().toString(), false),
            new UserId("2",UUID.randomUUID().toString(),false),
            new UserId("3",UUID.randomUUID().toString(),false)};


    @Timeout(TIMEOUT)
    @DisplayName("onDisconnectFromLocalhost")
    @Test
    void onDisconnectFromLocalhost() throws URISyntaxException, InterruptedException {
        SocketIOClient client = spy(SocketIOClient.class);
        UserId userId=new UserId("dest1", UUID.randomUUID().toString(),true);
        when(dataService.findUserIdBySessionId(anyString())).thenReturn(Optional.of(userId));
        doNothing().when(dataService).deleteUserId(anyString());
        doNothing().when(connectListener).onConnect(any());
        doNothing().when(client).sendEvent(anyString(), any());
;       when(dataService.findUserFriends(anyString())).thenReturn(Arrays.asList(array));
        when(clientManager.getClient(any())).thenReturn(client);
        when(clientManager.isNotNull(any())).thenReturn(true);
        final BlockingQueue<Object> values = new LinkedBlockingQueue<Object>();
        socket.on(Socket.EVENT_CONNECT, objects -> {
            socket.on(Socket.EVENT_DISCONNECT, args ->{
                values.offer("done");
            } );
            socket.close();
        });
        socket.connect();
        values.take();
        verify(client,times(3)).sendEvent(anyString(),any());
    }
    @AfterEach
    void after() {
        socket.disconnect();
    }

}