print('===============JAVASCRIPT===============');
print('Count of rows in test collection: ' + db.msgdb.count());

db.msgdb.insert({ myfield: 'test1', anotherfield: 'TEST1' });
db.msgdb.insert({ myfield: 'test2', anotherfield: 'TEST2' });

print('===============AFTER JS INSERT==========');
print('Count of rows in test collection: ' + db.msgdb.count());

alltest = db.msgdb.find();
while (alltest.hasNext()) {
  printjson(alltest.next());
}
